package com.formationandroid.kotlinprojetfinal.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.formationandroid.kotlinprojetfinal.model.database.DAO.MemoDAO
import com.formationandroid.kotlinprojetfinal.model.database.DTO.MemoDTO


@Database(entities = [MemoDTO::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun memoDAO(): MemoDAO
}
