package com.formationandroid.kotlinprojetfinal.model.database.DAO

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.formationandroid.kotlinprojetfinal.model.database.DTO.MemoDTO


@Dao
abstract class MemoDAO {

    @Query("SELECT * FROM MemoDTO")
    abstract fun getListememo(): MutableList<MemoDTO>

    @Insert
    abstract fun insert(vararg memo: MemoDTO?)

    @Delete
    abstract fun delete(vararg memo: MemoDTO?)

}