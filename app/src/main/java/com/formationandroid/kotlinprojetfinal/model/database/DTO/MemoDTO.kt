package com.formationandroid.kotlinprojetfinal.model.database.DTO

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class MemoDTO(titre: String) {

    @PrimaryKey(autoGenerate = true)
    var memoId : Long = 0
    var titre: String = titre


}