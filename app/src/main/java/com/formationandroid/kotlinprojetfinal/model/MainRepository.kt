package com.formationandroid.kotlinprojetfinal.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.formationandroid.kotlinprojetfinal.model.dI.DIApplication
import com.formationandroid.kotlinprojetfinal.model.database.AppDataBaseHelper
import com.formationandroid.kotlinprojetfinal.model.database.DAO.MemoDAO
import com.formationandroid.kotlinprojetfinal.model.database.DTO.MemoDTO
import javax.inject.Inject

class MainRepository {

    @Inject
    lateinit var context: Context

    @Inject
    lateinit var memoDAO: MemoDAO

    init {
        DIApplication.getAppComponent()?.inject(this);

    }


    fun getLiveDataMemo(liveDataItem: MutableLiveData<MutableList<MemoDTO>>) {
        val listTitre: MutableList<MemoDTO> = memoDAO.getListememo()
        liveDataItem.value = listTitre; }


    // add memo
    fun createDataMemo(string: String) {
        memoDAO.insert(MemoDTO(string))
    }

    // delete memo
    fun deleteDataMemo(memoDTO: MemoDTO){
        memoDAO.delete(memoDTO)
    }

    // get memo
    fun getDataMemo(): MutableList<MemoDTO> {
        return memoDAO.getListememo()
    }


}