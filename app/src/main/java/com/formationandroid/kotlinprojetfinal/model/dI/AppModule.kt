package com.formationandroid.kotlinprojetfinal.model.dI

import android.app.Application
import android.content.Context
import androidx.annotation.Nullable
import com.formationandroid.kotlinprojetfinal.model.MainRepository
import com.formationandroid.kotlinprojetfinal.model.database.AppDataBaseHelper
import com.formationandroid.kotlinprojetfinal.model.database.DAO.MemoDAO
import com.formationandroid.kotlinprojetfinal.viewModel.MainViewModel
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    companion object {
        @Provides
        fun provideContext(@Nullable application: Application): Context {
            return application
        }

        @Provides
        fun provideMainViewModel(): MainViewModel {
            return MainViewModel()
        }

        @Provides
        fun provideMainRepository(): MainRepository {
            return MainRepository()
        }

        @Provides
        fun provideDAO(context: Context): MemoDAO {
            return AppDataBaseHelper.getDatabase(context)!!.memoDAO()
        }

    }

}