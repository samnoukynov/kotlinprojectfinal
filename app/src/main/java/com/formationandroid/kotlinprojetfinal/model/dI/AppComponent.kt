package com.formationandroid.kotlinprojetfinal.model.dI

import android.app.Application
import com.formationandroid.kotlinprojetfinal.model.MainRepository
import com.formationandroid.kotlinprojetfinal.view.MainActivity
import com.formationandroid.kotlinprojetfinal.viewModel.ItemTouchHelperCallback
import com.formationandroid.kotlinprojetfinal.viewModel.ListAdapterMemo
import com.formationandroid.kotlinprojetfinal.viewModel.MainViewModel

import dagger.BindsInstance
import dagger.Component


@Component(modules = [AppModule::class ])
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application?): Builder?
        fun build(): AppComponent?
    }

    fun inject(itemTouchHelperCallback: ItemTouchHelperCallback)
    fun inject( mainRepository: MainRepository)
    fun inject( mainActivity: MainActivity)
    fun inject(mainViewModel: MainViewModel)
    fun inject(memoViewHolder: ListAdapterMemo.MemoViewHolder)


}