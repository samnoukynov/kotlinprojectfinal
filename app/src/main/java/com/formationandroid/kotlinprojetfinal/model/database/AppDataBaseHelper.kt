package com.formationandroid.kotlinprojetfinal.model.database

import android.content.Context
import androidx.room.Room

class AppDataBaseHelper  constructor(context: Context) {

    private var database: AppDatabase? = null

    init {
        database = Room.databaseBuilder(context, AppDatabase::class.java, "memo.db")
            .allowMainThreadQueries().build()
    }

    companion object {
        private var databaseHelper: AppDataBaseHelper? = null


        @Synchronized
        fun getDatabase(context: Context): AppDatabase? {
            if (databaseHelper == null) {
                databaseHelper =
                    AppDataBaseHelper(
                        context.applicationContext
                    )
            }
            return databaseHelper?.database
        }
    }

}