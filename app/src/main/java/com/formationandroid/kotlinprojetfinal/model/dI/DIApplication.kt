package com.formationandroid.kotlinprojetfinal.model.dI

import android.app.Application

class DIApplication : Application() {
    private var appComponent: AppComponent? = null
    override fun onCreate() {
        super.onCreate()
        instance = this
        appComponent = DaggerAppComponent.builder().application(this)?.build()
    }

    companion object {
        private var instance: DIApplication? = null
        fun getAppComponent(): AppComponent? {
            return instance!!.appComponent
        }
    }
}