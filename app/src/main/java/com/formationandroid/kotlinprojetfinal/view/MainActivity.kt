package com.formationandroid.kotlinprojetfinal.view

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.formationandroid.kotlinprojetfinal.R
import com.formationandroid.kotlinprojetfinal.model.dI.DIApplication
import com.formationandroid.kotlinprojetfinal.model.database.DTO.MemoDTO
import com.formationandroid.kotlinprojetfinal.viewModel.ItemTouchHelperCallback
import com.formationandroid.kotlinprojetfinal.viewModel.ListAdapterMemo
import com.formationandroid.kotlinprojetfinal.viewModel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    lateinit var editText:EditText ;
    lateinit var recyclerView: RecyclerView;
    private lateinit var listTitre: MutableList<MemoDTO>
    lateinit var itemTouchHelper: ItemTouchHelper

    @Inject
    lateinit var mainViewModel: MainViewModel


    init {
        DIApplication.getAppComponent()?.inject(this);
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        editText = this.findViewById(R.id.editText)
        recyclerView = findViewById(R.id.liste_memo)
        liste_memo.layoutManager = LinearLayoutManager(this)

        mainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        mainViewModel.getLiveData().observe(this, Observer {t: MutableList<MemoDTO> ->

            listTitre = t
            liste_memo.adapter = ListAdapterMemo(listTitre )
            liste_memo.adapter?.notifyDataSetChanged()

            // on détache le recyclerView du itemTouchHelper pour le réattacher
            // plus tard sur le nouveau itemTouchHelper
            itemTouchHelper.attachToRecyclerView(null);
            itemTouchHelper = ItemTouchHelper(ItemTouchHelperCallback(liste_memo.adapter as ListAdapterMemo))
            itemTouchHelper.attachToRecyclerView(recyclerView);
        })
        listTitre = mainViewModel.getListMemo()
        liste_memo.adapter = ListAdapterMemo(listTitre)
        itemTouchHelper = ItemTouchHelper(ItemTouchHelperCallback(liste_memo.adapter as ListAdapterMemo))
        itemTouchHelper.attachToRecyclerView(recyclerView);

    }

    fun addTitre(view: View) {

        mainViewModel.ajoutmemo(editText.text.toString())

    }


}


