package com.formationandroid.kotlinprojetfinal.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.formationandroid.kotlinprojetfinal.R
import com.formationandroid.kotlinprojetfinal.model.constant.MemoConstant

class DetailActivity : AppCompatActivity() {

    lateinit var textView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        textView = findViewById(R.id.textViewDetail)
    }

    override fun onStart() {
        super.onStart()
        textView.text = intent.getStringExtra(MemoConstant.MemoTitre)
    }

}
