package com.formationandroid.kotlinprojetfinal.viewModel

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.formationandroid.kotlinprojetfinal.R
import com.formationandroid.kotlinprojetfinal.model.constant.MemoConstant
import com.formationandroid.kotlinprojetfinal.model.dI.DIApplication
import com.formationandroid.kotlinprojetfinal.model.database.DTO.MemoDTO
import com.formationandroid.kotlinprojetfinal.view.DetailActivity
import java.util.*
import javax.inject.Inject


class ListAdapterMemo(private val list: MutableList<MemoDTO>) :
    RecyclerView.Adapter<ListAdapterMemo.MemoViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemoViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return MemoViewHolder(inflater, parent, list)
    }

    override fun onBindViewHolder(holder: MemoViewHolder, position: Int) {
        val memo: MemoDTO = list[position]
        holder.bind(memo)
    }

    override fun getItemCount(): Int = list.size

    fun getItem(position: Int): MemoDTO {
        return list[position]
    }


    public fun onItemMove(positionDebut: Int, positionFin: Int): Boolean {
        Collections.swap(list, positionDebut, positionFin);
        notifyItemMoved(positionDebut, positionFin);
        return true;
    }

    public fun onItemDismiss(position: Int) {
        if (position > -1) {
            list.removeAt(position);
            notifyItemRemoved(position); }
    }

    class MemoViewHolder(inflater: LayoutInflater, parent: ViewGroup, list: List<MemoDTO>) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_recycler_view, parent, false)) {
        private var mTitleView: TextView? = null
        lateinit var textViewLandscape: TextView
        @Inject
        lateinit var context: Context


        init {
            DIApplication.getAppComponent()?.inject(this);
            mTitleView = itemView.findViewById(R.id.titre_memo)
            mTitleView?.run {
                setOnClickListener {
                    // on regarde si l'écran est plus large de 600 pixel et si on est en landscape
                    // si oui on peut charger le textView de notre layout avec les qualifiers
                    // sinon on charge une activité
                    val orientation = resources.configuration.orientation;
                    val displayMetrics = DisplayMetrics()
                    (context as Activity).windowManager
                        .defaultDisplay
                        .getMetrics(displayMetrics)
                    val width = displayMetrics.widthPixels
                    if (width > 600 && orientation == 2) {
                        textViewLandscape = (context as Activity).findViewById(R.id.textViewLandscape)
                        textViewLandscape.text = list[adapterPosition].titre

                    } else {
                        val intent = Intent(context, DetailActivity::class.java)
                        intent.putExtra(MemoConstant.MemoTitre, list[adapterPosition].titre)
                        context.startActivity(intent)
                    }
                }
            };
        }

        fun bind(memo: MemoDTO) {
            mTitleView?.text = memo.titre

        }
    }

}
