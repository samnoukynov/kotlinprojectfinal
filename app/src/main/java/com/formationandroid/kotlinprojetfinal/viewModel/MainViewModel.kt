package com.formationandroid.kotlinprojetfinal.viewModel

import android.content.ClipData.Item
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.formationandroid.kotlinprojetfinal.model.MainRepository
import com.formationandroid.kotlinprojetfinal.model.dI.DIApplication
import com.formationandroid.kotlinprojetfinal.model.database.DTO.MemoDTO
import javax.inject.Inject


class MainViewModel: ViewModel() {
    private lateinit var liveDataItem: MutableLiveData<MutableList<MemoDTO>>

    @Inject
    lateinit var mainRepository: MainRepository

    init {
        DIApplication.getAppComponent()?.inject(this);
        properInit()
    }

    fun properInit() {
        liveDataItem = MutableLiveData()
    }

    fun getLiveData(): LiveData<MutableList<MemoDTO>> {
       return liveDataItem
    }

    fun ajoutmemo(string: String) {
        mainRepository.createDataMemo(string)
        mainRepository.getLiveDataMemo(liveDataItem)
    }

    fun deleteMemo(memoDTO: MemoDTO)
    {
        mainRepository.deleteDataMemo(memoDTO)
        mainRepository.getLiveDataMemo(liveDataItem)
    }


    fun getListMemo(): MutableList<MemoDTO> {
        return mainRepository.getDataMemo()
    }

}