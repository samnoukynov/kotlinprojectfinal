package com.formationandroid.kotlinprojetfinal.viewModel

import android.content.Context
import android.util.Log
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.formationandroid.kotlinprojetfinal.model.MainRepository
import com.formationandroid.kotlinprojetfinal.model.database.AppDataBaseHelper
import com.formationandroid.kotlinprojetfinal.model.dI.DIApplication
import javax.inject.Inject

class ItemTouchHelperCallback(private val adapter: ListAdapterMemo) : ItemTouchHelper.Callback() {

    @Inject
    lateinit var context: Context
    @Inject
    lateinit var mainViewModel: MainViewModel

    init {
        DIApplication.getAppComponent()?.inject(this);

    }

    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {
        val dragFlagsUpDown = 0;
        val dragFlagsRight = ItemTouchHelper.RIGHT
        return makeMovementFlags(dragFlagsUpDown, dragFlagsRight)
    }


    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        adapter.onItemMove(viewHolder.adapterPosition, target.adapterPosition)
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

        var positionItem: Int = viewHolder.adapterPosition
        mainViewModel.deleteMemo(adapter.getItem(positionItem))
        adapter.onItemDismiss(positionItem);
    }

    override fun isLongPressDragEnabled(): Boolean {
        return true
    }

    override fun isItemViewSwipeEnabled(): Boolean {
        return true
    }


}